import base62
import bottle
from bottle import hook, static_file
import pymongo, random, validators

"""
Set index at launch by getting the value of the last inserted document
"""


# db
# source, destination

HOST='http://uanow.org/'

client = pymongo.MongoClient('mongodb://localhost:27017/')
db = client['shurly']

@hook('after_request')
def enable_cors():
    bottle.response.headers['Access-Control-Allow-Origin'] = '*'
    bottle.response.headers['Access-Control-Allow-Methods'] = 'GET,POST,OPTIONS'
    bottle.response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'



@bottle.route('/')
def home_page():
    return bottle.template('dropin')

@bottle.route('/static/:path#.+#', name='static')
def static(path):
    return static_file(path, root='./static')

@bottle.route('/shurly/api/shorten', method=['OPTIONS', 'POST'])
def shrink_url():
    """
    Given a url, return a shrink url
    - First check if LONG_URL does no already exist
    """
    LONG_URL = bottle.request.query.longUrl
    SHORT_URL = bottle.request.query.shortUrl

    # If no URL is given to shorten, abort
    if LONG_URL == '':
        return "`longUrl` empty"

    # If the URL is not valid, abort
    if validators.url(LONG_URL) != True:
        return "URL is not valid. Remember to add `http://`"

    DOC = db.redirects.find_one({'destination': LONG_URL})

    # IF BY CHANCE GENERATED SHORT_URL ALREADY EXISTS, THIS MIGHT OVERRIDE THE
    # CURRENT URL IN DB
    try:
        # check if base62(URL_INDEX) already exists
        DOC['destination']
        if SHORT_URL == '':
            SHORT_URL = DOC['source']

    except Exception as e:
        if SHORT_URL == '':
            SHORT_URL = base62.encode((random.randint(238328,14776335)))
        print "Generated SHORT_URL: " + SHORT_URL
        # print "exception: ", type(e), e
        DOC = {'source':SHORT_URL, 'destination': LONG_URL}
        db.redirects.insert_one(DOC)

    rv = {"shortUrl":HOST+SHORT_URL}
    return rv

@bottle.route('/<SHORT_URL>')
def expand_url(SHORT_URL=''):
    """
    MongoDB lookup via short_url paramater and return the url
    """

    # Check chars in SHORT_URL are in base62
    for i in list(set(SHORT_URL)):
        if i not in base62.basedigits:
            return 'Invalid chars in url'

    # Try to lookup LONG_URL to redirect to
    try:
        DOC = db.redirects.find_one({'source': SHORT_URL})
        LONG_URL = DOC['destination']
        print "Found " + LONG_URL + " from " + SHORT_URL
    except Exception as e:
        print "LONG_URL not found for " + SHORT_URL, type(e), e
        return 'Error'

    bottle.redirect(LONG_URL, 301)


bottle.run(host='localhost', port=8005, debug=True)
