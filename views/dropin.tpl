<style>
.centered {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}
</style>

<img class="centered" src="https://digi.arizona.edu/emergency.jpg" />